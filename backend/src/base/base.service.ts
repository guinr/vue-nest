import { Injectable } from '@nestjs/common';
import { BaseDto } from 'shared/dto/base';

@Injectable()
export abstract class BaseService<T extends BaseDto> {

    abstract validate(dto: T): void;

    abstract create(dto: T);

    abstract findAll();

    abstract findById(id: string);

    abstract update(id: string, dto: T);

    abstract delete(id: string);

}

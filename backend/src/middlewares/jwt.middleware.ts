import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class JwtMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    console.log('req.route', req.route);
    console.log('req.body', req.body);
    next();
  }
}

import { Controller, Post, Get, Param, Patch, Delete, Body } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from 'shared/dto/user';

@Controller('user')
export class UserController {

    constructor(private readonly service: UserService) {}

    @Post('login')
    login(@Body() user: UserDto) {
        return this.service.login(user);
    }

    @Post()
    async create(@Body() user: UserDto) {
        return await this.service.create(user);
    }

    @Get()
    async findAll() {
        return await this.service.findAll();
    }

    @Get(':id')
    async findById(@Param('id') id: string) {
        return await this.service.findById(id);
    }

    @Patch(':id')
    async update(@Param('id') id: string, @Body() user: UserDto) {
        await this.service.update(id, user);
        return null;
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        await this.service.delete(id);
        return null;
    }

}

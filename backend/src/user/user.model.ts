import * as mongoose from 'mongoose';
import { UserInterface } from 'shared/dto/user';

export const UserModel = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true, select: false }
});

export interface UserModelInterface extends UserInterface, mongoose.Document {}

import { Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common';
import { UserDto } from 'shared/dto/user';
import { BaseService } from '../base/base.service';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserModelInterface } from './user.model';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService extends BaseService<UserDto> {

    constructor(@InjectModel('User') private readonly userModel: Model<UserModelInterface>) {
        super();
    }

    validate(dto: UserDto): void {
        if (!this.isValidUser(dto)) {
            throw new NotAcceptableException('Invalid fields');
        }
    }

    async create(dto: UserDto) {
        this.validate(dto);

        const password = await bcrypt.hash(dto.password, 14);

        const newUser = new this.userModel({
            name: dto.name,
            email: dto.email,
            password: password
        });

        const result = await newUser.save();

        return result.id as string;
    }

    async findAll() {
        return await this.userModel.find().exec() as UserDto[];
    }

    async findById(id: string) {
        let user: UserModelInterface;
        try {
            user = await this.userModel.findById(id).exec();

            if (!user) {
                throw new NotFoundException('User not found');
            }

            return user;
        } catch (ex) {
            throw new NotFoundException('User not found');
        }
    }

    async update(id: string, dto: UserDto) {
        const updatedUser = await this.findById(id);

        if (dto.name) {
            updatedUser.name = dto.name;
        }

        if (dto.email) {
            updatedUser.email = dto.email;
        }

        return await updatedUser.save();
    }

    async delete(id: string) {
        const result =  await this.userModel.deleteOne({ _id: id }).exec();

        if (result.deletedCount === 0) {
            throw new NotFoundException('User not found');
        }

        return null;
    }

    async login(user: UserDto) {

        if (!this.isValidLogin(user)) {
            throw new NotAcceptableException('Invalid fields');
        }

        const foundUser = await this.userModel.findOne({ email: user.email }).select('+password').exec();
        if (!foundUser) {
            throw new NotFoundException('User or password inválid');
        }

        const isPasswordMatching = await bcrypt.compare(user.password, foundUser.password);

        if (!isPasswordMatching) {
            throw new NotFoundException('User or password inválid');
        }

        // Gerar JWT
        return true;
    }

    private isValidUser(user: UserDto) {
        return user.name !== undefined &&
        user.email !== undefined &&
        user.password !== undefined &&
        user.name !== '' &&
        user.email !== '' &&
        user.password !== '';
    }

    private isValidLogin(user: UserDto) {
        return user.email !== undefined &&
        user.password !== undefined &&
        user.email !== '' &&
        user.password !== '';
    }

}

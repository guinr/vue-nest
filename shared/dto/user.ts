import { BaseDto } from './base'

export class UserDto extends BaseDto implements UserInterface {
    constructor(
        public id?: string,
        public name?: string,
        public email?: string,
        public password?: string
    ) {
        super();
    }
}

export interface UserInterface {
    name?: string,
    email?: string,
    password?: string
}
